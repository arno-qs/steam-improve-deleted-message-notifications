// ==UserScript==
// @name         Help with "moved/deleted" notifications
// @namespace    http://tampermonkey.net/
// @version      1.0.0
// @description  Help with "moved/deleted" notifications
// @author       Arno (https://steamcommunity.com/profiles/76561197974046543)
// @match        https://steamcommunity.com/profiles/*/commentnotifications/
// @grant        none
// ==/UserScript==

// Try and add a little usefulness to Valve's "This item was moved or deleted."
// comment notifications.
///////////////////////////////////////////////////////////////////////////////

(function() {
    'use strict'
    let deleted_notifications = document.getElementsByClassName("deleted")
    for (let i = 0; i < deleted_notifications.length; i++) {
        let magic_number = deleted_notifications[i].getAttribute("onclick").split("'")[7]
        let new_node = document.createElement("div")
        new_node.innerText = "You can try searching your browser history for the following string to try and locate the preceding thread: " + magic_number
        deleted_notifications[i].parentNode.insertBefore(new_node, deleted_notifications[i].nextSibling);
    }
})();