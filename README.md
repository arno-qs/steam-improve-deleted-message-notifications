Improve moved/deleted comment notifications
===========================================

First off, thanks to this post for providing the information I used to create
this userscript (and parts of this README):

https://steamcommunity.com/discussions/forum/10/135508031953236963/?ctp=9#c1762481957322857933

This userscript tries to help a little with Valve's cryptic "This item was moved
or deleted." comment notification messages.  On the surface, it's impossible to
see _what_ was moved or deleted, so you can't really take any action on or learn
anything from the action.

When you get one of these messages, an extra blurb will be added below the
message with an ID number extracted from the Javascript event handler text (you
can see the number in the HTML source).  You can search your browser history for
this string, and if you're lucky you'll get a match and can see which thread it
was.

Details and speculation
-----------------------

Let's start this section off by acknowledging that nothing here is certain,
because Valve doesn't confirm anything or respond to forum posts.  That said,
the below seems a) reasonable and b) consistent with the results I've seen when
testing.

There are several things that will result in these notifications:

- A moderator deletes a thread
- A moderator moves a thread to a forum you don't have access to read
- A user deletes their own thread

If you find a match in your browser history for the string provided, you can try
following the link to see if it provides more information on what happened:

- The thread actually loads: this probably means the thread was relocated
  (because it was off-topic where it was posted, for example).  You'll probably
  notice the forum ID in the URI is different from the one in your browser
  history.
- Redirects to the top page of a forum: the thread was deleted from that forum
- Redirects to a thread URI with the forum ID of "22": that's the moderators'
  quarantine/back room/whatever forum. That means something went seriously wrong
  in that thread.  These URIs will start with:
  `https://steamcommunity.com/discussions/forum/22/`